
variable "os" {
    description = "Which os do you want across these nodes? \n rhel, debian or ubuntu?"
}
variable "zone" {
  default = "us-west2-a"
}

variable "machine" {
    description = "micro or standard"
}
variable "count_of_instances" {
    description = "How many Instances"
}

variable os-type {
    description = "This maps to the various OSes of the Instances"
    type = "map"
    default = {
        rhel   = "rhel-7"
        ubuntu = "ubuntu-1810-cosmic" #-v20190402"
        debian = "debian-9-stretch" #-v20190423"
    }
}
variable "machine-type" {
    description = "This maps to the various OSes of the Instances"
    type = "map"
    default = {
        micro   = "f1-micro"
        standard = "n1-standard"
    }
}

variable "local_ssh_key" {
    default = "/home/ev/.ssh/id_rsa.pub"
}