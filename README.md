To run this set of scripts, make sure to export the `GOOGLE_CLOUD_KEYFILE_JSON` environment variable (and in a `.zshrc` or `.bashrc` to persist sessions) :

`export GOOGLE_CLOUD_KEYFILE_JSON='/path/to/service-account.key'`

- `terraform init`
- `terraform plan`
- `terraform apply`

```
The Terraform plan should provision 5 different compute engine instances from scratch with persistent disks and maps to pick OS/machine types
Feel free to expand on that for extra credit!
```

    Add ssh access to the Google Cloud Engine instance

You will need to add a public ssh key to the Google Cloud Engine instance to access and manage it. Add the local location of your public key to the google_compute_instace metadata in main.tf to add your ssh key to the instance. More information on managing ssh keys is available here.

resource "google_compute_instance" "default" {
 ...
metadata {
   sshKeys = "INSERT_USERNAME:${file("~/.ssh/id_rsa.pub")}"
 }
}
get list of gcloud machine-types
`gcloud compute images list --filter="zone:(us-west-1a) name~'standard'"`
get list of gcloud compute images"
`gcloud compute images list --filter="name~'ubuntu'"`
