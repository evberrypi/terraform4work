
# module "frontend" {
#   source = "/modules/microservice/main.tf"
# }

resource "google_compute_instance" "testing" {
  count        = "${var.count_of_instances}"
  name         = "cattle-${count.index}"
  machine_type = "${lookup(var.machine-type , var.machine)}"
  zone         = "${var.zone}"

# tags = ["SampleTag",  "tags" ]

boot_disk {
    initialize_params {
        image = "${lookup(var.os-type , var.os)}"
    }
}
metadata = {
    sshKeys = "${var.local_ssh_key}"
}

metadata_startup_script = "echo terraform-works > success.md"

  network_interface {
    network = "default"

    access_config {
      // This block is here to provision an external IP address
    }
  }
}
resource "google_compute_disk" "test-disk" {
    count   = "${var.count_of_instances}"
    name    = "test-disk-data-cattle-${count.index}"
    type    = "pd-standard"
    zone    = "${var.zone}"
    size    = "5"
}


